--- Prix des vins en fonction des cépages utilisés dans la réalisation du vin ---
SELECT DISTINCT v.Id AS id_vin, v.Nom as nom_vin, c.Nom as nom_cepage, v.Prix
FROM Vin v
INNER JOIN parcelle_compose_vin pcv ON pcv.vin = v.Id
INNER JOIN Parcelle P ON P.Numero = pcv.parcelle
INNER JOIN Cepage c ON c.Nom = p.Cepage_parcelle
Order by v.Id;

--- Prix des vins en fonction des évènements climatiques apparus sur les parcelles qui ont été utilisé dans la réalisation du vin ---
SELECT DISTINCT v.Id AS id_vin, v.Nom, Prix, te.Nom as evenement, Intensite, te.Description
FROM Vin v
INNER JOIN parcelle_compose_vin pcv ON pcv.vin = v.Id
INNER JOIN Parcelle P ON P.Numero = pcv.parcelle
INNER JOIN Impacte i ON i.parcelle = p.Numero
INNER JOIN Climat c ON c.Id = i.climat
INNER JOIN Possede_evenement pe ON pe.evenement = c.Id
INNER JOIN Type_evenement te ON te.Nom = pe.type
Order by v.Id;

--- Prix des vins en fonction de la Mode de Culture ainsi que la mode de récolte utilisées sur les parcelles et cépages---
SELECT DISTINCT v.Id AS id_vin, v.Nom, v.Prix, p.Cepage_parcelle as cépage, mdc.Type as culture, mdr.Methode_de_recolte as type_de_recolte
FROM Vin v
INNER JOIN parcelle_compose_vin pcv ON pcv.vin = v.Id
INNER JOIN Parcelle P ON P.Numero = pcv.parcelle
INNER JOIN Cepage c ON c.Nom = p.Cepage_parcelle
INNER JOIN Methode_de_recolte mdr ON mdr.Methode_de_recolte = p.recolte
INNER JOIN Mode_de_Culture mdc ON p.culture = mdc.Type
Order by v.Id;

--- Notation des vins en fonction des cépages utilisés dans la réalisation du vin ---
SELECT DISTINCT v.Id AS id_vin, v.Nom, p.Cepage_parcelle, cq.Notation as note_du_vin, cq.Decription_texture, cq.Decription_saveur, cq.Decription_global
FROM Vin v
INNER JOIN parcelle_compose_vin pcv ON pcv.vin = v.Id
INNER JOIN Parcelle P ON P.Numero = pcv.parcelle
INNER JOIN Cepage c ON c.Nom = p.Cepage_parcelle
INNER JOIN Critere_Qualitatif cq ON cq.Id = v.Critere
Order by v.Id;

--- Notation des vins en fonction des cépages utilisés dans la réalisation du vin ---
SELECT DISTINCT v.Id AS id_vin, v.Nom, p.Cepage_parcelle as cépage, cq.Notation
FROM Vin v
INNER JOIN parcelle_compose_vin pcv ON pcv.vin = v.Id
INNER JOIN Parcelle P ON P.Numero = pcv.parcelle
INNER JOIN Cepage c ON c.Nom = p.Cepage_parcelle
INNER JOIN Critere_Qualitatif cq ON cq.Id = v.Critere
Order by v.Id;

--- Afficher les vins ayant le prix le plus haut ---
SELECT DISTINCT v.Nom, MAX(v.Prix) as cout_max
FROM Vin v
GROUP BY v.Nom;

--- Afficher le vin ayant le prix le plus bas ---
SELECT DISTINCT v.Nom, MIN(v.Prix) as cout_min
FROM Vin v
GROUP BY v.Nom;

--- Affiche le prix total qu'à rapporté un vin ---
SELECT DISTINCT v.Nom, ROUND(SUM(v.Prix)::numeric, 2) as cout_total
FROM Vin v
GROUP BY v.Nom;

--- Nombre de parcelles en fonction de chaque cépage (seulement pour les cépages concernant au moins 2 parcelles) ---
SELECT c.Nom as nom_cepage, COUNT(p.Numero) as nombre_parcelle
FROM Cepage c 
INNER JOIN Parcelle p ON p.Cepage_parcelle = c.Nom
GROUP BY c.Nom
HAVING COUNT(p.Numero) > 1;

--- Nombre de bouteilles vendues en fonction des prix des vins (en arrondissant les prix à l'euro, pour les vins supérieurs à 10 €) ---
SELECT ROUND(v.Prix) as Prix_vin, SUM(cv.Nombre_bouteille_vendue) as nb_bouteille_vendue
FROM Circuit_Vente cv
INNER JOIN Vin v ON v.Circuit = cv.Id
GROUP BY ROUND(v.Prix)
HAVING ROUND(v.Prix) > 9;

--- Note moyenne en fonction des lieux ---
SELECT ROUND(AVG(cq.Notation),2) as Note_Moyenne, cq.Lieu
FROM Critere_Qualitatif cq
GROUP BY cq.Lieu;

--- Nombre de traitements par parcelle ---
SELECT t.parcelle as num_parcelle, COUNT(t.traitement)
FROM Traite t
GROUP BY t.parcelle
ORDER BY 1;

--- Prix du vin moyen en fonction du contenant---
SELECT co.Contenant, ROUND(AVG(v.Prix)::numeric, 2) as Prix_Moyenne
FROM Conditionnement co
INNER JOIN Vin v ON v.Conditionnement_Vin = co.Id
GROUP BY co.Contenant
ORDER BY 1;

--- Coût total des traitements et du coût de récolte utilisés sur une parcelle ---
SELECT p.Numero as num_parcelle, ROUND((SUM(t.Prix) + mr.Prix)::numeric, 2) as Prix_total
FROM Traite tr
INNER JOIN Traitement t ON t.Nom = tr.traitement
INNER JOIN Parcelle p ON p.Numero = tr.parcelle
INNER JOIN Methode_de_recolte mr ON mr.Methode_de_recolte = p.Recolte
GROUP BY p.Numero, mr.Prix
ORDER BY 1;
