# Note de Clarification - Exploitation viticole

## Parcelle
 - Numéro (entier > 0)
 - Cepage
 - Surface (m²)
 - Exposition (1 - 10)
 - Type du sol {argileuse, sableuse, calcaire}
 - Modeculture
 - Mode de taille
 - Mode de Récolte
 - Traitement

## Méthode de récolte
 - Méthode de récolte {Manuel, mécanique}
 - Prix de la méthode (euro/heure)

## Mode de Culture
 - Type (désherbées en plein, enherbées et tondues, ou cultivées)
 - Description

## Type de sol
 - Nom
 - Irrigation

## Mode de taille
 - Nom

## Cepage
 - Nom
 - Couleur du fruit
 - Durée de germination

## Traitement
 - Nom
 - Description du produit
 - Cible du traitement
 - Prix du traitement
 - Durée du traitement

## Vin
 - Nom
 - Critère qualitatif
 - Conditionnement
 - Prix

## critère qualitatif
 - Teinte de la robe
 - Description Saveur (strict, acide, salé, amer, sucrosité..)
 - Description Texture (Aqueux, vin qui a de la matière..)
 - Description Général
 - Notation général (1 - 10)
 - Pourcentage d'alcool
 - Date de mise en bouteille
 - Lieu de production

## Conditionnement
 - Nom (Bouteille, Bidon, Fût)
 - Contenant {75cl, 1l, 1.50l, 2l, 5l, 10l}

## Circuit de Vente
 - Nom
 - Nombre de bouteille vendue

## Climat
 - Type d'évènement
 - Date de début
 - Date de fin
 - intensité (1 à 10)

## Type d'évènement
 - Nom
 - Description

## Hypothèse
 - Il n'y a que 1 type de cepage par parcelle
 - Il n'y a que 1 type de sol par parcelle
 - Les critères qualitatifs sont déterminés par l'analyse et la dégustation mais ne sont pas présente dans la BDD
 - Pour une parcelle, l'exposition au soleil est notée entre 1 et 1. (1 = mal exposé,  10 = très bien exposé)
 - Une parcelle possède un numéro unique.
 - La surface d'une parcelle est en m².
 - Une parcelle possède obligatoirement un seul cépage.
 - Une parcelle possède au moins un mode de culture.
 - Le mode de culture est soit désherbées en plein, enherbées et tondues, ou cultivées.
 - Pour le mode de culture, de taille et le traitement phytosanitaire, les descriptions servent de notice/instructions.
 - Un vin possède un nom unique choisi par l'exploitant.
 - Chaque vin est analysé et se voit donner une note sur 10 qui est la moyenne des notes critères qualitatifs (1 = mauvais,  10 = très bon)
 - Un événement climatique est noté entre 1 et 10. (1 = peu intense, 10 = très intense)
 - Un événement climatique peut être de plusieurs types.
