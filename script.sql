---	Suppression des Tables	---
DROP TABLE IF EXISTS Parcelle CASCADE;
DROP TABLE IF EXISTS Cepage CASCADE;
DROP TABLE IF EXISTS Type_de_sol CASCADE;
DROP TABLE IF EXISTS Traitement CASCADE;
DROP TABLE IF EXISTS Mode_de_Culture CASCADE;
DROP TABLE IF EXISTS Methode_de_recolte CASCADE;
DROP TABLE IF EXISTS Mode_de_taille CASCADE;
DROP TABLE IF EXISTS Critere_Qualitatif CASCADE;
DROP TABLE IF EXISTS Circuit_Vente CASCADE;
DROP TABLE IF EXISTS Conditionnement CASCADE;
DROP TABLE IF EXISTS Vin CASCADE;
DROP TABLE IF EXISTS Climat CASCADE;
DROP TABLE IF EXISTS Type_evenement CASCADE;
DROP TABLE IF EXISTS Traite CASCADE;
DROP TABLE IF EXISTS parcelle_compose_vin CASCADE;
DROP TABLE IF EXISTS Impacte CASCADE;
DROP TABLE IF EXISTS Possede_evenement CASCADE;

---	Suppression des Types	---
DROP TYPE IF EXISTS type_Culture CASCADE;
DROP TYPE IF EXISTS type_recolte CASCADE;
DROP TYPE IF EXISTS couleur_fruit CASCADE;

DROP SEQUENCE IF EXISTS num_parcelle CASCADE;
DROP SEQUENCE IF EXISTS Critere CASCADE;
DROP SEQUENCE IF EXISTS vente CASCADE;
DROP SEQUENCE IF EXISTS conditionnement_vin CASCADE;
DROP SEQUENCE IF EXISTS id_vin CASCADE;
DROP SEQUENCE IF EXISTS id_climat CASCADE;

---	Creation d'un Type	---

CREATE TYPE type_Culture as enum ('Desherbees en plein','Enherbees et tondues', 'Cultivees');
CREATE TYPE type_recolte as enum ('Manuel','Mecanique');
CREATE TYPE couleur_fruit as enum('Rouge','Blanc');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Cepage (
	Nom VARCHAR(45) PRIMARY KEY,
	Couleur_fruit couleur_fruit NOT NULL,
	Duree_germinaison INTEGER NOT NULL CHECK (Duree_germinaison > 0)
);

--- Insertion des donnees de test pour la table  ---
INSERT INTO Cepage VALUES ('Chardonnay', 'Blanc', 100),
							('Aligoté', 'Blanc',  65),
							('Chenin', 'Blanc',  90),
							('Marsanne', 'Blanc',  70),
							('Muscadet', 'Blanc',  86),	
							('Muscad blanc', 'Blanc',  75),	
							('Riesling', 'Blanc',  85),	
							('Semillion', 'Blanc',  93),	
							('Sauvignon', 'Blanc',  102),
							('Sylvaner', 'Blanc',  101),
							('Merlot', 'Rouge',  81),
							('Cabernet franc', 'Rouge',  95),
							('Cabernet sauvignon', 'Rouge',  87),
							('Malbec', 'Rouge',  105),
							('Petit verdot', 'Rouge',  102),
							('Tressot', 'Rouge',  88),
							('César', 'Rouge',  87),
							('Pinot noir', 'Rouge',  89),
							('Tannat', 'Rouge',  78),
							('Gamay', 'Rouge',  70);


----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Type_de_sol (
	Nom VARCHAR(45) PRIMARY KEY,
	Irrigation VARCHAR NOT NULL
);

INSERT INTO Type_de_sol VALUES ('Sablonneux', 'Tres faible'), 
								('Boueux', 'Importante'),
								('Limoneuse', 'Importante'),
								('Calcaire', 'Faible'),
								('tourbeuse', 'Moyenne'),
								('Sec', 'Tres faible'),
								('Argileux', 'Faible');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Traitement (
	Nom VARCHAR(45) PRIMARY KEY,
	Description VARCHAR NOT NULL,
	Cible VARCHAR NOT NULL,
	Prix FLOAT NOT NULL CHECK (Prix > 0),
	Duree_traitement INTEGER NOT NULL CHECK (Duree_traitement > 0)
);

INSERT INTO Traitement VALUES ('Vitamine', 'Permet à la plante de pousser plus vite', 'Plante entiere', 16.50, 20),
								('Antie-fourmie', 'Repousse les fourmies', 'fourmies', 40.00, 35),
								('Antie-sautrelles', 'Repousse les sautrelles', 'sautrelles', 25.60, 60); 

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Mode_de_Culture (
	Type type_Culture PRIMARY KEY,
	Description VARCHAR NOT NULL
);

INSERT INTO Mode_de_Culture VALUES ('Enherbees et tondues', 'Permet de réduire la prolifération des maladies'),
								('Desherbees en plein', 'Permet d intensifier l apport en eau et en mineraux à la terre'),
								('Cultivees', 'Culture Intensive');
								
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Methode_de_recolte (
	Methode_de_recolte type_recolte PRIMARY KEY,
	Prix FLOAT NOT NULL CHECK (Prix > 0) 
);

INSERT INTO Methode_de_recolte VALUES ('Manuel', 20.30),
								('Mecanique', 13.60);

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Mode_de_taille (
	Nom VARCHAR PRIMARY KEY,
	Description VARCHAR NOT NULL
);

INSERT INTO Mode_de_taille VALUES ('Taille courte', 'On ne garde qu’un à deux yeux par sarment.'),
									('Taille longue', 'On conserve de quatre à dix yeux par sarment.'),
									('Guyot simple', 'La baguette sera formee par le sarment superieur et le courson par le sarment inferieur.'),
									('Guyot double', 'La baguette sera formee par le sarment superieur et le courson par le sarment inferieur.');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

---	Creation des Tables	---

CREATE TABLE Parcelle (
	Numero INTEGER PRIMARY KEY,
	Surface INTEGER NOT NULL CHECK (Surface > 0),
	Exposition INTEGER NOT NULL CHECK ( Exposition > 0 AND Exposition <= 10),
	Cepage_parcelle VARCHAR(45),
	Type_de_sol VARCHAR(45) NOT NULL,
	Culture type_Culture,
	Recolte type_recolte,
	Taille VARCHAR,
	FOREIGN KEY (Cepage_parcelle) REFERENCES Cepage(Nom),
	FOREIGN KEY (Type_de_sol) REFERENCES Type_de_sol(Nom),
	FOREIGN KEY (Culture) REFERENCES Mode_de_Culture(Type),
	FOREIGN KEY (Taille) REFERENCES Mode_de_taille(Nom),
	FOREIGN KEY (Recolte) REFERENCES Methode_de_recolte(Methode_de_recolte)
);

--- Creation de la sequence pour la table  ---
CREATE SEQUENCE num_parcelle START 1;

---	Insertion des donnees de test pour la table	---
INSERT INTO Parcelle VALUES (nextval('num_parcelle'), 260, 3, 'Sauvignon', 'Sablonneux', 'Cultivees', 'Manuel', 'Taille courte'),
                            (nextval('num_parcelle'), 600, 1, 'Aligoté', 'Sec', 'Enherbees et tondues', 'Mecanique', 'Guyot simple'),
                            (nextval('num_parcelle'), 325, 6, 'Chardonnay', 'Sec', 'Enherbees et tondues', 'Mecanique', 'Taille courte'),
                            (nextval('num_parcelle'), 856, 3, 'Aligoté', 'tourbeuse', 'Enherbees et tondues', 'Manuel', 'Guyot simple'),
                            (nextval('num_parcelle'), 128, 2, 'Chardonnay', 'Sablonneux', 'Cultivees', 'Manuel','Taille courte'),
                            (nextval('num_parcelle'), 652, 8, 'Chenin', 'Boueux', 'Cultivees', 'Mecanique', 'Guyot simple'),
                            (nextval('num_parcelle'), 136, 9, 'Tannat', 'Argileux', 'Desherbees en plein', 'Mecanique', 'Guyot double'),
                            (nextval('num_parcelle'), 136, 9, 'Tannat', 'Argileux', 'Desherbees en plein', 'Manuel', 'Guyot simple'),
                            (nextval('num_parcelle'), 290, 10, 'Merlot', 'Calcaire', 'Cultivees', 'Manuel', 'Taille courte'),
                            (nextval('num_parcelle'), 427, 9, NULL, 'Argileux', 'Enherbees et tondues', NULL, NULL),
                            (nextval('num_parcelle'), 297, 4, NULL, 'Boueux', 'Enherbees et tondues', NULL, NULL),
                            (nextval('num_parcelle'), 797, 7, NULL, 'Boueux', 'Enherbees et tondues', NULL, NULL),
                            (nextval('num_parcelle'), 692, 4, NULL, 'Sec', 'Enherbees et tondues', NULL, NULL),
                            (nextval('num_parcelle'), 510, 5, 'Sauvignon', 'Sablonneux', 'Desherbees en plein', 'Mecanique', 'Guyot simple'),
                            (nextval('num_parcelle'), 267, 3, NULL, 'Boueux', 'Enherbees et tondues', NULL, NULL),
                            (nextval('num_parcelle'), 402, 1, 'Marsanne', 'Calcaire', 'Cultivees', 'Mecanique', 'Guyot double'),
                            (nextval('num_parcelle'), 402, 5, 'Chenin', 'Calcaire', 'Desherbees en plein', 'Manuel', 'Taille longue'),
                            (nextval('num_parcelle'), 402, 6, 'Marsanne', 'tourbeuse', 'Desherbees en plein', 'Mecanique', 'Taille longue'),
                            (nextval('num_parcelle'), 402, 3, 'Malbec', 'Sec', 'Cultivees', 'Manuel', 'Guyot simple'),
                            (nextval('num_parcelle'), 402, 7, 'Malbec', 'tourbeuse', 'Desherbees en plein', 'Manuel', 'Taille longue'),
                            (nextval('num_parcelle'), 402, 10, 'Aligoté', 'Sec', 'Cultivees', 'Mecanique', 'Guyot simple');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Critere_Qualitatif (
	Id INTEGER PRIMARY KEY,
	Couleur_robe VARCHAR NOT NULL,
	Decription_saveur VARCHAR NOT NULL,
	Decription_texture VARCHAR NOT NULL,
	Decription_global VARCHAR NOT NULL,
	Notation INTEGER NOT NULL CHECK (Notation > 0 AND Notation <= 10),
	Alcool FLOAT NOT NULL CHECK (Alcool > 0),
	Mise_en_bouteille DATE NOT NULL,
	Lieu VARCHAR NOT NULL
);

CREATE SEQUENCE Critere START 1;

INSERT INTO Critere_Qualitatif VALUES (nextval('Critere'), 'Couleur blanc pale', 'Saveur âpre', 'Texture aqueuse', 'Vin moyen', 5, 12.2, '12-11-1998', 'Bordeaux'),
									(nextval('Critere'), 'Couleur verdâtre', 'Saveur sucre', 'Texture Charpenté', 'Vin excellent', 9, 12.2, '03-06-2001', 'Marmande'),
									(nextval('Critere'), 'Couleur blanc translucide', 'Arômes floraux', 'Texture soyeux', 'Vin mauvais', 3, 14.3, '09-04-2000', 'Bourgogne'),
									(nextval('Critere'), 'Couleur verdâtre', 'Arômes épicés', 'Texture aqueuse', 'Vin bon', 9, 10.2, '10-04-2009', 'Bourgogne'),
									(nextval('Critere'), 'Couleur jaune vieil or', 'Saveur sucre', 'Texture velouté', 'Vin bof', 6, 13.69, '09-06-2009', 'Beaujolais'),
									(nextval('Critere'), 'Couleur jaune vieil or', 'Arômes boisés', 'Texture soyeux', 'Vin bon', 4, 13.52, '09-07-2001', 'Beaujolais'),
									(nextval('Critere'), 'Couleur blanc translucide', 'Saveur sucre', 'Texture Charpenté', 'Vin tres bon', 10, 11.52, '21-12-2009', 'Marmande'),
									(nextval('Critere'), 'Couleur rouge rubis', 'Arômes chimiques', 'Texture soyeux', 'Vin mauvais', 3, 14.20, '04-01-2006', 'Bourgogne'),
									(nextval('Critere'), 'Couleur violacé', 'Arômes minéraux', 'Texture limpide', 'Vin excellent', 8, 9.75, '09-04-2007', 'Marmande'),
									(nextval('Critere'), 'Couleur rouge grenat', 'Arômes boisés', 'Texture velouté', 'Vin tres bon', 8, 14.01, '20-03-2001', 'Bordeaux'),
									(nextval('Critere'), 'Couleur vin noir', 'Arômes minéraux', 'Texture limpide', 'Vin excellent', 9, 13.00, '09-06-2008', 'Alsace');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Circuit_Vente (
	Id INTEGER PRIMARY KEY,
	Nom VARCHAR(45) NOT NULL,
	Nombre_bouteille_vendue INTEGER NOT NULL CHECK (Nombre_bouteille_vendue > 0)
);

CREATE SEQUENCE vente START 1;

INSERT INTO Circuit_Vente VALUES (nextval('vente'), 'Particulier', 30),
									(nextval('vente'), 'Auchan', 1120),
									(nextval('vente'), 'Super U', 3000),
									(nextval('vente'), 'Carrefour', 1205),
									(nextval('vente'), 'Auchan', 2015),
									(nextval('vente'), 'Carrefour', 3025),
									(nextval('vente'), 'Auchan', 5000),
									(nextval('vente'), 'Particulier', 15),
									(nextval('vente'), 'Particulier', 11),
									(nextval('vente'), 'Particulier', 25),
									(nextval('vente'), 'Particulier', 30);

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Conditionnement (
	Id INTEGER PRIMARY KEY,
	Nom VARCHAR(45) NOT NULL,
	Contenant FLOAT NOT NULL CHECK (Contenant > 0)
);

CREATE SEQUENCE conditionnement_vin START 1;

INSERT INTO Conditionnement VALUES (nextval('conditionnement_vin'), 'Bouteille', 1.50),
									(nextval('conditionnement_vin'), 'Bouteille', 0.75),
									(nextval('conditionnement_vin'), 'Bouteille', 1.25),
									(nextval('conditionnement_vin'), 'Tonneau', 50),
									(nextval('conditionnement_vin'), 'Tonneau', 60),
									(nextval('conditionnement_vin'), 'Tonneau', 45),
									(nextval('conditionnement_vin'), 'Bouteille', 1.75),
									(nextval('conditionnement_vin'), 'Bidon', 3.50),
									(nextval('conditionnement_vin'), 'Bidon', 4.0),
									(nextval('conditionnement_vin'), 'Bidon', 3.25),
									(nextval('conditionnement_vin'), 'Bidon', 3.0);

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Vin (
	Id INTEGER PRIMARY KEY,
	Nom VARCHAR NOT NULL,
	Prix FLOAT NOT NULL CHECK (Prix > 0),
	Conditionnement_Vin INTEGER NOT NULL,
	Critere INTEGER NOT NULL,
	Circuit INTEGER NOT NULL,
	FOREIGN KEY (Conditionnement_Vin) REFERENCES Conditionnement(Id),
	FOREIGN KEY (Critere) REFERENCES Critere_Qualitatif(Id),
	FOREIGN KEY (Circuit) REFERENCES Circuit_Vente(Id)
);

CREATE SEQUENCE id_vin START 1;

INSERT INTO Vin VALUES (nextval('id_vin'), 'Bordeaux', 6.52, 1, 1, 10),
						(nextval('id_vin'), 'Monbazillac', 45.50, 11, 3, 8),
						(nextval('id_vin'), 'Sauvignon', 3.20, 10, 4, 7),
						(nextval('id_vin'), 'Sauvignon', 60.20, 10.50, 5, 6),
						(nextval('id_vin'), 'Bordeaux', 102, 5, 2, 9),
						(nextval('id_vin'), 'Monbazillac', 100.99, 1, 6, 5),
						(nextval('id_vin'), 'Monbazillac', 30.20, 7, 7, 4),
						(nextval('id_vin'), 'Beaujolais-villages', 40.50, 8, 8, 3),
						(nextval('id_vin'), 'Blaye', 15.20, 9, 9, 2),
						(nextval('id_vin'), 'Anjou mousseux', 20.78, 8, 10, 1),
						(nextval('id_vin'), 'Monbazillac', 60.30, 9, 11, 11);

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Climat (
	Id INTEGER PRIMARY KEY,
	Date_debut DATE NOT NULL,
	Date_fin DATE NOT NULL,
	Intensite INTEGER NOT NULL CHECK (Intensite > 0 AND Intensite <= 10)
);

CREATE SEQUENCE id_climat START 1;

INSERT INTO Climat VALUES (nextval('id_climat'), '22-11-2016', '22-12-2016', 3),
							(nextval('id_climat'), '06-08-2017', '27-09-2017', 6),
							(nextval('id_climat'), '25-02-2019', '02-03-2019', 6),
							(nextval('id_climat'), '20-08-2018', '27-09-2018', 4),
							(nextval('id_climat'), '15-02-2019', '20-02-2019', 8),
							(nextval('id_climat'), '03-08-2017', '27-09-2017', 4),
							(nextval('id_climat'), '09-05-2019', '20-05-2019', 7);

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Type_evenement (
	Nom VARCHAR(25) PRIMARY KEY,
	Description VARCHAR NOT NULL
);

INSERT INTO Type_evenement VALUES ('Orage', 'Tres grosse precipitation'),
									('Pluie', 'Precipitation'),
									('Innondation', 'Vignes noyees'),
									('Secheresse', 'Manque d eau');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Traite (
	parcelle INTEGER,
	traitement VARCHAR(45),
	PRIMARY KEY (parcelle, traitement),
	FOREIGN KEY (parcelle) REFERENCES Parcelle(Numero),
	FOREIGN KEY (traitement) REFERENCES Traitement(Nom)
);

INSERT INTO Traite VALUES (1, 'Vitamine'),
							(1, 'Antie-fourmie'),
							(2, 'Vitamine'),
							(3, 'Vitamine'),
							(3, 'Antie-fourmie'),
							(3, 'Antie-sautrelles');

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE parcelle_compose_vin (
	vin INTEGER,
	parcelle INTEGER,
	PRIMARY KEY (parcelle, vin),
	FOREIGN KEY (parcelle) REFERENCES Parcelle(Numero),
	FOREIGN KEY (vin) REFERENCES Vin(Id)
);

INSERT INTO parcelle_compose_vin VALUES (1, 1),
										(1, 2),
										(1, 4),
										(2, 6),
										(2, 2),
										(2, 1),
										(2, 5),
										(3, 3),
										(3, 8),
										(4, 4),
										(4, 6),
										(5, 3),
										(5, 7),
										(5, 5),
										(5, 4),
										(6, 6),
										(6, 7),
										(6, 8),
										(7, 5),
										(7, 3),
										(8, 2),
										(9, 1),
										(9, 2),
										(10, 4),
										(10, 1),
										(11, 5),
										(11, 8);
										
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE TABLE Impacte (
	parcelle INTEGER,
	climat INTEGER,
	PRIMARY KEY (parcelle, climat),
	FOREIGN KEY (climat) REFERENCES Climat(Id),
	FOREIGN KEY (parcelle) REFERENCES Parcelle(Numero)
);

INSERT INTO Impacte VALUES (1, 2),
							(2, 5),
							(3, 4),
							(4, 2),
							(5, 2),
							(6, 3),
							(10, 3);

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
		
CREATE TABLE Possede_evenement (
	type VARCHAR(25),
	evenement INTEGER,
	PRIMARY KEY (type, evenement),
	FOREIGN KEY (type) REFERENCES Type_evenement(Nom),
	FOREIGN KEY (evenement) REFERENCES Climat(Id)
);

INSERT INTO Possede_evenement VALUES ('Orage', 1),
										('Innondation', 2),
										('Orage', 4),
										('Innondation', 5),
										('Pluie', 4),
										('Pluie', 3);
